import React from 'react';
import Link from 'next/link';

const Footer = ({views = 'Error'}) => (
    <footer>
        <div className="f-left">
            <div className="icon-n-text">
                <img src="fb-logo.png"></img>
                <a href="https://www.facebook.com"> Contacto de facebook</a>
            </div>
            <div className="icon-n-text">
                <img src="ph-logo.png"></img>
                <a>5537593177</a>
            </div>
        </div>
        <div className="f-center">
            <p>Visitas en el sitio: {views}</p>
        </div>
        <div className="f-right">
            <p>Av. Juan de Dios Bátiz S/N, Nueva Industrial Vallejo, Gustavo A. Madero, 07738 Ciudad de México, CDMX</p>
        </div>
        <style jsx>{`
        a, a img {
            outline: none;
        }
        footer{
            background: #a51212;
            color: #f8f8ff;
            grid-area: footer;
            display: grid;
            grid-template-columns: 30% 30% 40%;
            grid-template-areas: 'left center right';
        }
        footer p{
            text-size: 15px;
        }
        .f-left{
            vertical-align: middle;
            padding-left: 20px;
            grid-area: left;
        }
        .f-center{
            grid-area: center;
            font-size: 20px;
        }
        .f-right{
            padding-right: 20px;
            grid-area: right;
        }
        .icon-n-text img{
            position: relative;
            height: 25px;
            top: 6px;
        }
        .icon-n-text a{
            margin-left: 10px;
        }
        .icon-n-text a:link, .icon-n-text a:visited {
            color: #f8f8ff;
            text-decoration: none;
        }
        `}</style>
    </footer>
);

export default Footer;