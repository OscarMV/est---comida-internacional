import React from 'react';
import Link from 'next/link';

const Header = () => (
    <header>
        <div className="h-left">
            <img src="logo.png"></img>
            <a href="/">Cocina internacional</a>
        </div>
        <div className="h-right">
            <div className="dropdown">
                <a className="button">Continentes</a>
                <div className="dropdown-content">
                    <a href="/america">America</a>
                    <a href="/asia">Asia</a>
                    <a href="/africa">Africa</a>
                    <a href="/europa">Europa</a>
                    <a href="/oceania">Oceania</a>
                </div>
            </div>
            <a  className="button" href="#">Nosotros</a>
        </div>
        <style jsx>{`
        a, a img {
            outline: none;
        }
        header{
            grid-area: header;
            background: #a51212;
            color: #f8f8ff;
            display: grid;
            grid-template-columns: reteat(50%, 2);
            grid-template-areas: 'left right';
        }
        .h-left{
            grid-area: left;
        }
        .h-left img{
            margin-left: 40px;
            margin-top: 5px;
            height: 90px;
            vertical-align:middle;
        }
        .h-left a{
            font-size: 40px;
            position: absolute;
            top: 28px;
            left: 145px;
        }
        .h-left a:link, .h-left a:visited {
            color: #f8f8ff;
            text-decoration: none;
        }
        .h-right{
            text-align: right;
            padding-right: 15px;
            margin-top: 40px;
            grid-area: right;
        }
        .h-right a{
            font-size: 20px;
            color: #f8f8ff;
            cursor: pointer;
            border-radius: 5px;
            padding: 20px;
            text-decoration: none;
        }
        .h-right a:hover{
            background: #ee2e31;
        }
        .dropdown {
            position: relative;
            display: inline-block;
        } 
        .dropdown-content {
            display: none;
            position: absolute;
            top: 40px;
            right:0;
            border-radius: 4px;
            background: #f8f8ff;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }
        
        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            display: block;
            text-align: left;
        }
        
        .dropdown-content a:hover {
            background-color: #f1f1f1
        }
        
        .dropdown:hover .dropdown-content {
          display: block;
        }

        .dropdown:hover .button {
            background-color: #ee2e31;
        }
        `}</style>
    </header>
)

export default Header;