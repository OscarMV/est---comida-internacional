import React from 'react';

import Header from '../components/header';
import Footer from '../components/footer';

const America = ({views}) => (
  <div>
    <div className="wrapper">
      <Header />
      <main>
        <h1>America</h1>
        <h3>Gastronimía de américa</h3>
        <hr></hr>
        <p>
        Hablar de la gastronomía de América, es hablar de gran diversidad de componentes, alimentos, técnicas, y sobre todo influencias culturales y socioeconómicas.
        América es el segundo continente más grande del planeta, después de Asia.
        </p>
        <h3>América del norte o norteamérica</h3>
        <hr></hr>
        <p>América del Norte o Norteamérica es un subcontinente que forma parte de América, situado en el Hemisferio Norte. Limita al norte con el océano Glacial Ártico, al este con el océano Atlántico, al sureste con el mar Caribe y al sur y al oeste con el océano Pacífico. Está conectado con América Central. </p>
        <p>La Gastronomía de América del Norte hablamos históricamente una gran diversidad de componentes, alimentos, técnicas, y sobre todo influencias culturales y socioeconómicas que han determinado la cultura gastronómica de los diferentes países que lo componen.</p>
        <p>Podría decirse que la gastronomía es la de los Indios Nativos en el continente y el resto de gastronomía es la fusión aportada por años de colonización de diferentes culturas dando lugar a la cocina actual estadounidense que recoge variantes aderezas de todas ellas.</p>
        <h4><span>Paises que comparten america del norte</span>Canadá, Estados Unidos y México.</h4>
        <h4>Canadá</h4><hr></hr>
        <div className="wrapper2">
          <div className="w2-left america1">
            <img src="america1.jpg"></img>
          </div>
          <div className="w2-right">
            <p>En Canadá no se puede hablar de una cocina propia que abarque todo país, sino que cada región tiene una tradición gastronómica particular que utiliza lo mejor de los productos de la tierra dándoles un toque personal. Debido a inmenso tamaño de Canadá, se da gran una variedad de tradiciones culinarias que como denominador común tienen su pasado colonial.</p>
            <p>La Gastronomía de Canadá es un conjunto de costumbres culinarias que corresponden a los habitantes de Canadá, las costumbres difieren bastante según las regiones de las que se trate, existen muchas influencias de la cocina estadounidense e inglesa, aunque las zonas francófonas tienen influencias o reminiscencias de platos de la cocina francesa.</p>
          </div>
        </div>
        <h4>Estados unidos</h4><hr></hr>
        <div className="wrapper2">
          <div className="w2-left">
            <p>La gastronomía de los Estados Unidos corresponde a una mezcla muy variada y algo interpretada de otras gastronomías nacionales procedentes de diferentes países de Europa, Asia, África, y otros países.</p>
            <p>Dado el gran tamaño del territorio de los Estados Unidos no es de sorprender que la cocina sea diversa y que pueda ser tipificada en variantes regionales. La cocina de la Costa Este, por ejemplo, hace uso como ingredientes del pescado y de los mariscos mucho más que las cocinas del Medio Oeste, donde el maíz y la carne de vacuno tienen una mayor preponderancia y se dispone mejor de ellas.</p>
          </div>
          <div className="w2-right america2">
            <img src="america2.jpg"></img>
          </div>
        </div>
        <h4>México</h4><hr></hr>
        <div className="wrapper2">
          <div className="w2-left america3">
            <img src="america3.jpg"></img>
          </div>
          <div className="w2-right">
            <p>La gastronomía mexicana, con su gran variedad de platillos tradicionales regionales, es rica en gusto, olor y colorido, orgullo del patrimonio cultural de la nación; atributos que atraen por sí solo al turista.</p>
            <p>Su cocina cuenta con recetas milenarias que han ido pasando de generación en generación y que hoy en día constituyen un atractivo para el denominado Turismo Gastronómico, el cual ha ido incrementando su importancia en el mercado mundial.
            Casi cada estado mexicano posee sus propias recetas y tradiciones culinarias. Desde luego esta diversidad es más notoria si se contempla la riqueza gastronómica regionalmente y no por entidad federativa. Hay ciertas creaciones gastronómicas que surgieron localmente y que por su calidad y aceptación generalizada se han vuelto emblemáticas de la cocina mexicana en lo general.</p>
          </div>
        </div>
        <h3>América central o Centroamérica</h3><hr></hr>
        <p>América Central, también llamada Centroamérica o América del Centro, es un subcontinente que conecta América del Norte con América del Sur. Rodeada por el océano Pacífico y el océano Atlántico. Políticamente se divide en los siete países independientes de Guatemala, Belice, Honduras, El Salvador, Nicaragua, Costa Rica y Panamá.</p>
        <h4><span>Paises que comparten centroamérica:</span>Guatemala, Belice, Honduras, El Salvador, Nicaragua, Costa Rica y Panamá.</h4>
        <div className="america4"><img src="america4.jpg"></img></div>
        <h3>América del Sur o Latinoamérica</h3><hr></hr>
        <p>América del Sur o Sudamérica, también llamada Suramérica, anteriormente conocida simplemente como América (1507–1538), es el subcontinente austral de América.</p>
        <p>Está atravesada por la línea ecuatorial en su extremo norte, quedando así con la mayor parte de su territorio comprendida dentro del Hemisferio Sur.
        Está situada entre el océano Atlántico y el océano Pacífico quienes delimitan los extremos Este y Oeste respectivamente, mientras que el Mar Caribe delimita por el norte y el Océano Antártico su extremo sur.</p>
        <p>Ocupa una superficie de 17,8 millones de km², lo que representa un 42% del continente americano y un 12% de las tierras emergidas,10 y está habitada por el 6% de la población mundial.</p>
        <div className="america4"><img src="america5.jpg"></img></div>
        <h4><span>Paises que comparten america del sur:</span>Argentina, Bolivia, Brasil, Chile, Colombia, Costa Rica, Cuba, Ecuador, El Salvador, Guatemala, Haití, Honduras, México, Nicaragua, Panamá, Paraguay, Perú, Puerto Rico, República Dominicana, Uruguay, y Venezuela.</h4>
        <div className="wrapper2">
          <div className="w2-left america4">
            <img src="america7.jpg"></img>
          </div>
          <div className="w2-right america4">
          <img src="america6.jpg"></img>
          </div>
        </div>
        <p>Sintetizar aún someramente la enorme composición de alimentos, culturas y su propia gastronomía es imposible en esta amalgama tan extensa de territorio, historia y tradiciones del Continente Americano. Pero si podemos hacer un repaso somero e indicativo de los diferentes países y regiones.</p>
        <div className="wrapper2">
          <div className="w2-left america8">
            <img src="america8.jpg"></img>
          </div>
          <div className="w2-right">
            <p>Y casi el conjunto de todas o parte de esas gastronomías, la podemos sintetizar en lo que se vendría a llamar Gastronomía Iberoamericana.</p>
            <p>La gastronomía es un fenómeno cultural que ha tenido un desarrollo particular en los países iberoamericanos. Las costumbres gastronómicas de un país dicen mucho de su historia, de su idiosincrasia y del momento presente por el que atraviesa. Como en otros órdenes, la originalidad y el mestizaje han marcado a la cocina de las naciones iberoamericanas.
            Así, por ejemplo, la cocina latinoamericana es la suma de muchas influencias, pero hay dos fundamentales: la gastronomía indígena y la que aportaron los europeos. De ambas nació la cocina criolla que, a su vez, se vio enriquecida con la llegada de inmigrantes europeos y asiáticos entre finales del siglo XIX y el primer tercio del XX.</p>
          </div>
        </div>
        <br></br>
        <div className="wrapper2">
          <div className="w2-left">
            <p>La cocina indígena tenía su base en la caza, la pesca y en determinados productos agrícolas como maíz, calabaza, alubias, azúcar del arce etc. Para los ingredientes básicos eran las papas (patatas), el maíz y la yuca que acompañaban a la carne y los diversos pescados. En México los aztecas se alimentaban de gallinas, gallos de papada (pavos), tórtolas, perdices, que junto con cebollas, berros, hacederas, ajíes o chiles, ciruelas componían su alimentación.</p>
          </div>
          <div className="w2-right america9">
            <img src="america9.jpg"></img>
          </div>
        </div>
        <p>Si la tradición local se basaba en el maíz, las patatas, los tomates, los frijoles o el chocolate, la tradición europea aportó gallinas, cerdos, ovejas, vacas, trigo, vino, naranjas, limones y otras frutas. La mezcla de ambas tradiciones provocó el nacimiento de una gastronomía de mestizaje y muy variada. A esta mezcla se unió la llegada desde África de productos como el ñame, el coco y toda la variedad de bananos y plátanos. La influencia europea, española y portuguesa, se vio reforzada por la italiana (especialmente en Argentina y Uruguay), la cocina francesa y otras más actuales como la asiática o la india.</p>
        <div className="america4"><img src="america10.jpg"></img></div>
        <p>En América latina, Perú y México son los cocinas más fuertes y conocidas internacionalmente. La cocina mexicana está basada en el maíz y cuenta, además, con una gran riqueza de gastronomía indígena y popular, así como cultivos autóctonos que se nutren de las especies vegetales y animales de México.</p>
        <div className="america4"><img src="america11.jpg"></img></div>
        <h4>Bolivia</h4><hr></hr>
        <div className="wrapper2">
          <div className="w2-left">
            <p>Cocina boliviana, mezcla de criolla y de andina, en la que destacan sus sopas o chupes, sus platos basados en el cerdo, el maíz y la papa, sus asados picantes, sus humitas (masa de maíz envuelta en su propia hoja y cocida) o las conocidas como salteñas, una empanada nacida en Sucre que lleva carne molida, patatas, guisantes, pasas, aceitunas, huevo duro y ají.</p>
          </div>
          <div className="w2-right america12">
            <img src="america12.jpg"></img>
          </div>
        </div>
        <h4>Perú</h4><hr></hr>
        <div className="wrapper2">
          <div className="w2-left america13">
            <img src="america13.jpg"></img>
          </div>
          <div className="w2-right">
            <p>La variedad gastronómica existente en Perú, cocina de la costa, de la sierra y la criolla, junto con las influencia oriental y africana han transformado a Perú en un referente de la cocina latinoamericana: sobresalen los ceviches (pescado crudo macerado), el ají de gallina, los anticuchos (brochetas de corazón de vaca muy troceado, a la parrilla), la causa limeña (puré de patatas, ajíes, huevos duros y aceitunas), las papas a la huancaína (rellenas con picadillo de carne) o los dulces de escuela española (bienmesabe, picarones, tocino de cielo).</p>
            <p>Para beber sobresale el pisco sour (cóctel de pisco, zumo de limón, azúcar y clara de huevo). La cocina peruana es, por lo tanto, considerada una de las más variadas del mundo pues une a las herencias pre incaica, incaica, española, africana, asiática, italiana y francesa.</p>
          </div>
        </div>
        <h4>Brasil</h4><hr></hr>
        <p>Brasil tiene a gala contar con una gastronomía igualmente amplia fruto de numerosas influencias: la indígena, la africana o la portuguesa. Internacionalmente, lo más conocido es la churrasquería, estilo rodizio. Junto a las ensaladas, destaca el plato nacional, la feijoada, guiso de frijoles negros con cerdo que se acompaña de arroz, harina de mandioca y naranja pelada. También destacan los galetos (pollitos despiezados) o la picanha (corte de vaca).</p>
        <div className="america4"><img src="america14.jpg"></img></div>
        <h4>Colombia</h4><hr></hr>
        <div className="wrapper2">
          <div className="w2-left america15">
            <img src="america15.jpg"></img>
          </div>
          <div className="w2-right">
            <p>La riqueza gastronómica colombiana es una de las mayores de América. Internacionalmente, lo más conocido es su cocina del interior: arepas (tortas de maíz), y los guisos de influencia española, en donde se incluyen ingredientes autóctonos como el maíz, el plátano verde, el ñame y la papa. Asimismo cabe destacar la cocina antioqueña, costeña, cundiboyacense, opita, pastusa, santandereana, valluna, etc. Entre los platos más destacados se encuentran el sancocho, la bandeja paisa y el ajiaco.</p>
            <p>Lo colonial se impone a lo indígena en la gastronomía venezolana, con un mestizaje en el que se aúnan lo salado y lo picante, lo dulce con lo agrio. Las recetas más sobresalientes son el pabellón caraqueño, sancocho de pescado, o la sopa de quinbombó</p>
          </div>
        </div>
        <h4>Ecuador</h4><hr></hr>
        <p>Ecuador tiene su propia personalidad culinaria más allá de las influencias peruana y colombiana. En este aspecto destacan las ensaladas, aguado (caldo con bolas de plátano verde), llapingachos (tortillas de patata con queso y salsa de maní), empanaditas de mote (maíz blanco) o la fanesca (guiso con maíz, guisantes, frijoles, lentejas, etc.).</p>
        <div className="america4"><img src="america16.jpg"></img></div>
        <h4>Uruguay</h4><hr></hr>
        <p>De la misma manera, Uruguay, pese a estar entre dos colosos gastronómicos como Brasil y Argentina, tiene su propia personalidad culinaria en donde los asados o parrilladas se convierten en los platos estrella. Como plato diferenciador destaca el chivito, consistente en lomito de vaca con queso, lechuga, tomate y huevo duro. En Paraguay, por su parte, debe quedar reseñado la chipa también llamada Pan paraguayo.</p>
        <h4>Argentina</h4><hr></hr>
        <div className="wrapper2">
          <div className="w2-left">
            <p>La gastronomía argentina se caracteriza y diferencia de las del resto de América por la gran influencia de las dos corrientes migratorias que la conformaron como nación: los inmigrantes italianos y los españoles. Existe, en el interior del país, ejemplos de pervivencia de la tradición indígena, sobre todo, en el área andina y en la zona guaraní. En Argentina, destaca la tradición de las parrillas, el consumo de pastas de influencia italiana (ravioles, ñoquis, pizzas) y las empanadas de procedencia gallega.</p>
          </div>
          <div className="w2-right america17">
            <img src="america17.jpg"></img>
          </div>
        </div>
        <p>El postre nacional son los panqueques (filloas rellenas de dulce de leche) y dulces como los alfajores. Para beber, el tradicional mate y su vinos de la región de Mendoza, principalmente. En resumen, pueden distinguirse cuatro regiones gastronómicas en este país: la Central y Pampeana, la de Noroeste y Cuyo, la del Noreste, y, por último la de la Patagonia y Tierra del Fuego.</p>
        <h4>Chile</h4><hr></hr>
        <p>La comida chilena tiene algunos platos de gran importancia como las empanadas, el pastel de choclos, las humitas, los porotos granados y el curanto. Asimismo, destacan los mariscos tales como locos, machas, choros, centolla, y pescados como el congrio, salmón, corvina y lenguado. Los vinos y frutas de Chile, le han valido reconocimiento internacional por su calidad y buena selección. La bebida tradicional es el pisco, que se puede degustar preparado como pisco sour.</p>
        <h4>Cuba</h4><hr></hr>
        <div className="wrapper2">
          <div className="w2-left america18">
            <img src="america18.jpg"></img>
          </div>
          <div className="w2-right">
            <p>La gastronomía típica cubana es el resultado de la interacción de las influencias española, africana y la asiática. Los españoles llevaron a la dieta de la isla las legumbres, el arroz, las naranjas, los limones y el ganado vacuno. Los africanos incorporaron alimentos como el ñame, al que se sumaron los existentes en la isla, como la yuca, el quimbombó, el boniato o batata y el maíz. Todos estos elementos se unieron con el paso del tiempo formando así la actual cocina cubana. El típico plato cubano es el ajiaco, una sopa de viandas y carne. De influencia africana es el congrí.</p>
          </div>
        </div>
        <h4>Republica dominicana</h4><hr></hr>
        <p>Dado que se trata de un país de importante producción, la República Dominicana ofrece una gran variedad de platos donde se dan influencias taínas, europea y africana con platos como el chechén, típico del sur del país que consiste en maíz partido en trozos pequeños y hervido al que se acompaña de chivo guisado. El chacá, también a base de maíz, postre preparado con leche, azúcar y canela, así como leche de coco.</p>
        <p>El pescado y el moro de guandules con coco, típico de Samaná. El arroz con frijoles y el plátano, con el que se prepara el mangú, el mofongo y los pasteles en hoja, así como la yuca, con la que se elabora, además del casabe una empanada rellena de carne, queso o pollo denominada catibía.</p>
        <div className="america4"><img src="america19.jpg"></img></div>
        <h4>Cocina centroaméricana</h4><hr></hr>
        <div className="wrapper2">
          <div className="w2-left">
            <p>La cocina centroamericana ofrece asimismo una gran variedad de platos y en la preparación de la mayoría existen ingredientes comunes: maíz, yuca, papa, plátano, carnes y mariscos.El maíz, base de la cocina de los mayas, es el ingrediente clave en la gastronomía de la región en la que destacan el fiambre de Guatemala, la cazuela estilo hondureño, el pollo relleno con loroco de El Salvador, el típico nicaragüense, el tapado de camarón de Costa Rica.</p>
            <p>La variedad y el mestizaje son, asimismo, las características de la cocina española. Al igual que ocurre en la mayoría de países, la gastronomía de España es muy distinta de unas regiones a otras, aunque mantiene unos rasgos comunes y característicos como son el uso del aceite de oliva y la utilización de ajo y cebolla.</p>
          </div>
          <div className="w2-right america20">
            <img src="america20.jpg"></img>
          </div>
        </div>
        <p>Entre los principales platos españoles destacan la tortilla de patata, el gazpacho, la paella, embutidos como el jamón serrano, el chorizo y la morcilla o las diferentes modalidades de queso. Destacan asimismo los cocidos y potajes, así como las legumbres y la calidad de sus vinos.</p>
        <br></br><br></br>
      </main>
      <Footer views = {views}/>
    </div>

    <style jsx>{`
      :global(body) {
        margin: 0;
        padding: 0;
        background: #f8f8ff;
        font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
          Helvetica, sans-serif;
      }
      a, a img {
        outline: none;
      }
      .wrapper {
        display: grid;
        grid-template-columns: auto;
        grid-template-rows: 100px auto 70px;
        grid-template-areas: 'header' 'main' 'footer';
        width: 100%;
        height: 100vh;
      }
      header{
        grid-area: header;
      }
      main{
        grid:area: main;
        padding-left: 100px;
        padding-right: 100px;
      }
      main h1{
          font-size: 80px;
          text-align:center;
          margin: auto 40px;
      }
      main p{
          font-size: 18px;
      }
      main h3{
          font-size: 40px;
          margin: auto 10px;
          margin-top: 20px;
      }
      main h4{
        font-size: 30px;
        font-weight: 500;
        margin-bottom: 0;
      }
      main h4 span{
        font-weight: 600;
      }
      footer{
        grid-area: footer;
      }
      .wrapper2{
        margin-top: 20px;
        display: grid;
        grid-template-areas: 'left right';
      }
      .wrapper img{
        border-radius: 5px;
      }
      .wrapper2 p{
        margin: auto 30px;
      }
      .america1 img{
        height: 220px;
      }
      .america2 img{
        height: 190px;
      }
      .america3 img{
        height: 260px;
      }
      .america4{
        margin-top: 20px;
        text-align: center;
      }
      .america8 img{
        height: 290px;
      }
      .america9 img{
        height: 140px;
      }
      .america12 img{
        height: 100px;
      }
      .america13 img{
        height: 230px;
      }
      .america15 img{
        height: 200px;
      }
      .america17 img{
        height: 130px;
      }
      .america18 img{
        height: 200px;
      }
    `}</style>
  </div>
)

America.getInitialProps = async function() {
  const data = await import('../lib/views.json');
  return data;
};

export default America