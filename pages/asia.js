import React from 'react';

import Header from '../components/header';
import Footer from '../components/footer';

const Asia = ({views}) => (
  <div>
    <div className="wrapper">
      <Header />
      <main>
        <h1>Asia</h1>
        <p>La gastronomía asiática tiene más de 3000 años de antigüedad. Aunque es más conocida la China y japonesa, existen otros países cuyos platillos provocan una sensación agradable a nuestro paladar.</p>
        <p>En la comida asiática está muy presente el arroz, pues forma parte de los ingredientes de una gran cantidad y variedad de recetas pero también son imprescindibles en esta cocina las verduras.</p>
        <p>Es bien sabido que es un tipo de cocina muy sana pues usa poco aceite, carnes blancas y una gran variedad de frutas y verduras. Además se utilizan elementos para dar la sazón deseada como el curry y salsas para dar sabor especial a la carne.</p>
        <h3>Entre las salsas mas usadas se encuentran</h3>
        <hr></hr>
        <div className="small-list">
          <p>&#9974; <span>Salsa de sésamo: </span>preparada con diferentes aceites, soja y vinagre.</p>
          <p>&#9974; <span>Salsa agridulce: </span>se prepara con azúcar, vinagre y tomate.</p>
          <p>&#9974; <span>Salsa de soja: </span>sobradamente conocida incluso en occidente.</p>
          <p>&#9974; <span>Salsa teriyaki: </span>se prepara combinando la salsa agridulce y la de soja.</p>
        </div>
        <h3>Y entre los ingredientes</h3>
        <hr></hr>
        <div className="small-list">
          <p>&#9974; <span>Brotes de bambú: </span>este alimento está muy presente en la comida asiática.</p>
          <p>&#9974; <span>Col china: </span>tiene un sabor muy agradable y es un ingrediente muy usado en esta cocina.</p>
          <p>&#9974; <span>Algas: </span>En occidente recién estamos empezando a usarlas pero en comida asiática se usan desde hace mucho tiempo. Algas como hiziki, kombu, wakame, agar-agar o nori forman parte de muchos de sus platos.</p>
          <p>&#9974; <span>Salsas y especias: </span>las salsas y las especias tienen un papel muy importante en la cocina asiática, enriquecen y potencian el sabor de los platos y convierten una “simple receta” en un “gran manjar”</p>
          <p>&#9974; <span>Vegetales: </span>Los vegetales son ingredientes imprescindibles para la preparación de muchas de las recetas asiáticas.</p>
          <p>&#9974; <span>Soja: </span>la soja se usa para infinidad de composiciones ya que, además de ser una legumbre, se prepara tofu, aceite, leche, salsas…</p>
          <p>&#9974; <span>Pescados: </span>se utilizan mucho más que la carne tanto los de río como de mar. La carne no tiene un papel excesivamente protagonista en la comida asiática.</p>
        </div>
        <h3>Principales platillos asiaticos</h3>
        <h4>Bibimbap (Korea)</h4>
        <div className="wrapper2">
          <div className="w2-left asia1">
            <img src="asia1.jpg"></img>
          </div>
          <div className="w2-right">
            <p>Bibimbap es un plato popular coreano que significa "arroz mixto". Los ingredientes contienen verduras salteadas, carne a la parrilla y pasta de chile rojo, además de un huevo frito (o crudo), que se sirven en un tazón sobre arroz recién cocido. El comensal lo mezcla todo y disfruta de la explosión de sabores.</p>
          </div>
        </div>
        <h4>DIM SUM (CHINA)</h4>
        <div className="wrapper2">
          <div className="w2-left">
          <p>Plato cantonés que data de los tiempos cuando los viajeros tomaban un descanso para tomar té a lo largo de la Ruta de la Seda. El té se servía con pequeñas porciones de alimentos. Se trata de bolsitas de masa, fideos, carne, verduras y mariscos que se llevan a las mesas de los comensales.</p>
          </div>
          <div className="w2-right asia2">
            <img src="asia2.jpg"></img>
          </div>
        </div>
        <h4>Pancit (Filipinas)</h4>
        <div className="wrapper2">
          <div className="w2-left asia3">
            <img src="asia3.jpg"></img>
          </div>
          <div className="w2-right">
          <p>Muchos estadounidenses están familiarizados con la pho, la sopa vietnamita de fideos normalmente elaborada con carne, brotes de soja, cebolla y salsa hoisin. Esta sopa se inventó a principios del siglo XX cerca de Hanói. Cuando el país se dividió en Vietnam del Norte y Vietnam del Sur, la pho se difundió hacia el sur, y luego llegó a Estados Unidos en los años setenta y ochenta con los refugiados.</p>
          </div>
        </div>
      </main>
      <Footer views = {views}/>
    </div>

    <style jsx>{`
      :global(body) {
        margin: 0;
        padding: 0;
        background: #f8f8ff;
        font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
          Helvetica, sans-serif;
        color: #030007;
      }
      a, a img {
        outline: none;
      }
      .wrapper {
        display: grid;
        grid-template-columns: auto;
        grid-template-rows: 100px auto 70px;
        grid-template-areas: 'header' 'main' 'footer';
        width: 100%;
        height: 100vh;
      }
      header{
        grid-area: header;
      }
      main{
        grid:area: main;
        padding-left: 100px;
        padding-right: 100px;
      }
      footer{
        grid-area: footer;
      }
      main h1{
        font-size: 80px;
        text-align:center;
        margin: auto 40px;
      }
      main p{
        font-size: 18px;
      }
      main h3{
        font-size: 25px;
        margin: 20px auto;
      }
      main h4{
        font-size: 22px;
        margin: auto 20px;;
      }
      .small-list{
        margin: 0;
      }
      .small-list p{
        margin: 0;
        padding-left: 30px;
      }
      .small-list span{
        font-weight: 600;
      }
      .wrapper2{
        display: grid;
        grid-template-areas: 'left right';
      }
      .wrapper img{
        border-radius: 5px;
      }
      .wrapper2 p{
        margin: auto 30px;
      }
      .w2-left{
        grid-area: left;
        grid-area: right; 
      }
      .asia1 img{
        height: 300px;
      }
      .asia2 img{
        height: 150px;
      }
      .asia3 img{
        height: 300px;
      }
    `}</style>
  </div>
)

Asia.getInitialProps = async function() {
  const data = await import('../lib/views.json');
  return data;
};

export default Asia