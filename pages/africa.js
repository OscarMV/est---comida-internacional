import React from 'react';

import Header from '../components/header';
import Footer from '../components/footer';

const Africa = ({views}) => (
  <div>
    <div className="wrapper">
      <Header />
      <main>
        <h1>Africa</h1>
        <h3>Gastronimía africana</h3>
        <hr></hr>
        <p>
        El continente africano es el hogar de cientos de diferentes grupos culturales y étnicos. Esta diversidad también se refleja en las numerosas tradiciones culinarias locales en términos de elección de los ingredientes, el estilo de preparación y técnicas de cocina.
        </p>
        <p>Tradicionalmente,las diferentes cocinas de África utilizan una combinación de frutas disponibles en la localidad, los granos de cereales y verduras, así como los productos lácteos y la carne. En algunas partes del continente, la dieta tradicional cuenta con una preponderancia de los productos lácteos, cuajada y suero. En gran parte del África tropical, sin embargo, la leche de vaca es poco común y no pueden ser producidos localmente (debido a diversas enfermedades que afectan al ganado). Dependiendo de la región, también hay diferencias a veces muy importantes en los hábitos de comer y beber y tendencias a lo largo de muchas poblaciones del continente: África central, África oriental, el Cuerno de África, África del Norte, África del Sur y África Occidental tienen cada uno sus propios platillos.</p>
        <h3>Comida tipica</h3><hr></hr>
        <div className="small-list">
        <p className="list-title">&#9974; Cuscús:</p>
        <p className="list-element">Se trata del alimento básico en regiones del norte de África, tales como Marruecos, Túnez, Argelia y Egipto. A base de sémola de trigo y acompañado de diversos ingredientes como aceitunas, aceite de oliva, azafrán, nuez moscada, canela, jengibre, clavo de olor y otras especias típicas de la zona, éste es uno de los platillos más representativos de África.</p>
        <p className="list-title">&#9974; Ugali:</p>
        <p className="list-element">Este es otro de los más representativos y también se trata de un alimento a base de maíz, al almidón de maíz, el cual se sirve en sopas y guisos. Junto al matoke (una preparación de plátanos verdes al vapor) son la base de la alimentación del este de África, sobre todo en lugares como Uganda, donde se dice que se prepara el mejor ugali que pueda haber.</p>
        <p className="list-title">&#9974; Bambara:</p>
        <p className="list-element">El bambara es uno de los postres típicos de África, especialmente de las regiones centrales del continente. Se compone de un tipo de granola de arroz, mantequilla de maní y azúcar.</p>
        <p className="list-title">&#9974; Maafe:</p>
        <p className="list-element">El Maafe es básicamente un guiso de maní. Es muy popular en el oeste de África, donde se consume con frecuencia. Consiste en un estofado de carne, vegetales y maní, que se sirve con arroz y es decorado con huevo duro picado.</p>
        <p className="list-title">&#9974; Mariscos:</p>
        <p className="list-element">Los mariscos que preparan en el sur y en el este del continente.</p>
        <br></br><br></br><br></br>
        </div>
      </main>
      <Footer views = {views}/>
    </div>

    <style jsx>{`
      :global(body) {
        margin: 0;
        padding: 0;
        background: #f8f8ff;
        font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
          Helvetica, sans-serif;
      }
      a, a img {
        outline: none;
      }
      .wrapper {
        display: grid;
        grid-template-columns: auto;
        grid-template-rows: 100px auto 70px;
        grid-template-areas: 'header' 'main' 'footer';
        width: 100%;
        height: 100vh;
      }
      header{
        grid-area: header;
      }
      main{
        grid:area: main;
        padding-left: 100px;
        padding-right: 100px;
      }
      footer{
        grid-area: footer;
      }
      main h1{
        font-size: 80px;
        text-align:center;
        margin: auto 40px;
      }
      main p{
        font-size: 18px;
      }
      main h3{
        font-size: 40px;
        margin: auto 10px;
    }
      .list-element{
        padding-left: 30px;
        margin-top: 0;
      }
      .list-title{
          font-size: 24px!important;
          margin: 1px auto;
      }
    `}</style>
  </div>
)

Africa.getInitialProps = async function() {
  const data = await import('../lib/views.json');
  return data;
};

export default Africa