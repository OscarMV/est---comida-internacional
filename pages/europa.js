import React from 'react';

import Header from '../components/header';
import Footer from '../components/footer';

const Europa = ({views}) => (
  <div>
    <div className="wrapper">
      <Header />
      <main>
        <h1>Europa</h1>
        <h3>Gastronimía europea</h3>
        <hr></hr>
        <p>
        La geografía, el clima, y la herencia cultural, determinan la gastronomía de un país. Igual ocurre en cada país Europeo, aunque se trate de la misma receta, cada país tiene su propio sello y firma gastronómica hablando específicamente de pequeños detalles como lo pueden ser ingredientes adicionales a la receta original.
        </p>
        <h3>Caracteristicas de la gastronomia europea</h3>
        <hr></hr>
        <p>
        Esta consta de una gran variedad de recetas. Un mismo plato tiene su versión en otro país incluso de una región a otra, aunque esta cuenta con su toque exótico en los diversos platillos. Así mismo la variedad de climas que se encuentran en el continente Europeo permiten esa pluralidad no exenta de exotismo además de encontrarse la cocina mediterránea a la que ya se le ha dedicado un artículo por sus excelencias culinarias y saludables. 
        </p>
        <p>
        Una de las características más destacadas de la cocina Europea es que es rica en grasas y carnes principalmente; como ya antes se mencionó esto se debe al clima de la región. En invierno, por ejemplo, están más restringidos los cultivos y por esta razón las coles son una de las verduras mayormente consumidas. 
        </p>
        <h3>Ingredientes de la cocina europea</h3>
        <hr></hr>
        <p className="list-title">&#9974; Los condimentos:</p>
        <p className="list-element">Podemos encontrar platillos aromatizados con hierbas frescas o chiles. Jengibre, azafrán,canela, cardamomo, nuez moscada, clavos de olor, pimienta, ajedrea, cilantro, hinojo, mejorana, orégano,menta, mostaza, perejil, ruda, salvia, tomillo, etc.</p>
        <p className="list-title">&#9974; Carnes</p>
        <p className="list-element">Es más común consumir carnes que pescados. Las carnes de granja más utilizadas son: ternera,pollo, cerdo, conejo y cordero, etc. En determinadas zonas la carne de caza más utilizada es: ciervo, jabalí, codorniz, etc.</p>
        <p className="list-title">&#9974; Verduras y pastas</p>
        <p className="list-element">Papas, berenjena, cebolla, ajo, zanahoria, calabaza, calabacines, pepinos, lechugas, acelgas, tomates, espaguetis, macarrones y fideos.</p>
        <p className="list-title">&#9974; Cereales y legumbres</p>
        <p className="list-element">Maíz, trigo, avena,arroz, judías, garbanzos, lentejas y guisantes. Una de las gastronomías más fuertes de Europa son la Francesa y la Italiana; Francia cuenta con productos tan famosos como la mostaza de Dijon, mas 400 variedades de quesos, además de algunos vinos y champagnes más prestigiosos y cotizados del mundo. Mientras que la gastronomía Italiana nos ofrece las distintas variedades de pizzas y pastas, queso parmesano y mozzarella, risotto y polenta, el prosciutto, famoso jamón curado italiano.</p>
        <br></br>
      </main>
      <Footer views = {views}/>
    </div>

    <style jsx>{`
    :global(body) {
        margin: 0;
        padding: 0;
        background: #f8f8ff;
        font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
          Helvetica, sans-serif;
        color: #030007;
    }
    a, a img {
      outline: none;
    }
    .wrapper {
        display: grid;
        grid-template-columns: auto;
        grid-template-rows: 100px auto 70px;
        grid-template-areas: 'header' 'main' 'footer';
        width: 100%;
        height: 100vh;
    }
    header{
        grid-area: header;
    }
    main{
        grid:area: main;
        padding-left: 100px;
        padding-right: 100px;
    }
    main h1{
        font-size: 80px;
        text-align:center;
        margin: auto 40px;
    }
    main p{
        font-size: 18px;
    }
    main h3{
        font-size: 40px;
        margin: auto 10px;
    }
    footer{
        grid-area: footer;
    }
    .list-element{
        padding-left: 30px;
        margin-top: 0;
    }
    .list-title{
        font-size: 24px!important;
        margin: 1px auto;
    }
    `}</style>
  </div>
)

Europa.getInitialProps = async function() {
  const data = await import('../lib/views.json');
  return data;
};

export default Europa