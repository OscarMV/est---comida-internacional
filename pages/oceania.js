import React from 'react';

import Header from '../components/header';
import Footer from '../components/footer';

const Oceania = ({views}) => (
  <div>
    <div className="wrapper">
      <Header />
      <main>
        <h1>Oceania</h1>
        <h3>Gastronimía de oceania</h3>
        <hr></hr>
        <p>Occeania abarca desde Nueva Zelanda hasta Australia</p>
        <h3>Cocina de Nueva Zelanda</h3><hr></hr>
        <p>Nueva Zelanda se destaca por su enorme variedad de mariscos como son: tuatua,  paua y variedades de ostras, con estos hacen sopas de mariscos, también se encuentra el cerdo, venado, borlas, mejillones, almejas y kiwis  que es el más típico del país.</p>
        <h3>Cocina de Australia</h3><hr></hr>
        <p>En la mayor parte de la cocina australiana tiene un origen británico; pues este país fue colonia del Reino Unido.</p>
        <p>Los platos británicos más tradicionales son los pasteles de carne o pollo grillado, estos son los más comunes en el menú australiano, en especial la carne a la parrilla hecha al aire libre, incluyendo el cordero, cocodrilo, canguro y búfalo.</p>
        <p>Sobre la costa marítima se encuentra el pescado y los frutos de mar que son protagonistas de la cocina australiana en especial los langostinos o camarones y el cangrejo. 
        En los desayunos o postres se encuentra el tocino con huevos y la más tradicional torta de pavlova con merengue y frutas, esto sin duda es el postre excelente de Australia.</p>
        <p>Si algo caracteriza los productos de esta cocina, es sin lugar a dudas su frescura. Encontramos una gran variedad de frutas y verduras, aprovechadas evidentemente en muchos de sus platos. De estas, se podría destacar el sabor de sus mangos, papayas, papas o zanahorias. Asimismo, también es interesante destacar sus quesos, que aunque no tengan tanta fama como los quesos franceses, lo cierto es que disfrutan de una calidad exquisita y un sabor asombroso.</p>
        <p>La carne y pescado es fundamental en sus recetas. Además debemos recalcar que muchas de estas carnes pueden resultar un tanto exóticas. Todo sea dicho la mayoría de éstas eran ya usadas por los aborígenes de Australia desde hace aproximadamente 40.000 o 60.000 años.</p>
        <p>Si fuera de Australia podría resultar raro comer carne de canguro, por poner un ejemplo, aquí es algo completamente normal. Los restaurantes llenan sus menús con platos con éste, haciendo ver que la comida bushfood es ahora una moda gastronómica bastante usada en el país.</p>
        <p>Entre las carnes y pescados más usados en la gastronomía australiana podemos encontrar el ya mencionado canguro, pero también otros como el Emu, Barramundi, Trevalla, Moreton Bay Bug y el Yabby. Además no sería raro encontrar platos realizados con cucarachas (tipos especiales de este insecto, no todas) y escorpiones. Evidentemente estos dos últimos ingredientes sólo son aptos para aquellos que no sean escrupulosos con la comida.</p>
        <p>De entre esos alimentos y especias nativas que hemos resaltado podemos destacar el Akudjira, Davidson´s plum, Desert Lime, Finger Lime, Illawarra Plum, Lemon Aspen, Lemon Myrtle, Mountain Pepper, Macadamia, Muntries, Quandong, Riberry o el Wattleseed.</p>
        <p>Cabe destacar que la gran influencia inglesa ha dado como resultado una gran variedad de pasteles, es más, uno de sus platos más típicos es precisamente uno de estos pasteles, el delicioso pastel de carne.</p>
        <p>A pesar de que los postres no sean el fuerte de esta gastronomía, podemos encontrar algunos platos dulces que son muy populares y bastante peculiares.
        El rey de estos platos, o por lo menos el más típico, es el Pavlova. Este pastel está hecho a base de nata, merengue y fruta fresca como el kiwi, el melocotón, el mango etc.
        Además también se pueden destacar otros postres, igualmente deliciosos, como el Anzac biscuits,Lamingtons o la Vanilla slice.</p>
        <p>En cuanto a la bebida, cabe destacar que los vinos australianos gozan de gran fama a nivel internacional. Cuentan con una excelente calidad. Podemos destacar aquellos que surgen en el sur del país, en el valle Barossa, aunque también son deliciosos los vinos jóvenes del sudoeste. Australia cuenta con cuatro grandes bodegas Southcorp Wines, BRL-Hardy, Orlando Wyndhan y Mildara Blass.</p>
      </main>
      <Footer views = {views}/>
    </div>

    <style jsx>{`
      :global(body) {
        margin: 0;
        padding: 0;
        background: #f8f8ff;
        font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
          Helvetica, sans-serif;
      }
      a, a img {
        outline: none;
      }
      .wrapper {
        display: grid;
        grid-template-columns: auto;
        grid-template-rows: 100px auto 70px;
        grid-template-areas: 'header' 'main' 'footer';
        width: 100%;
        height: 100vh;
      }
      header{
        grid-area: header;
      }
      main{
        grid:area: main;
        padding-left: 100px;
        padding-right: 100px;
      }
      main h1{
          font-size: 80px;
          text-align:center;
          margin: auto 40px;
      }
      main p{
          font-size: 18px;
      }
      main h3{
          font-size: 40px;
          margin: auto 10px;
      }
      footer{
        grid-area: footer;
      }
    `}</style>
  </div>
)

Oceania.getInitialProps = async function() {
  const data = await import('../lib/views.json');
  return data;
};

export default Oceania