import React from 'react';

import Head from 'next/head';

//import M from "materialize-css";
//import "materialize-css/dist/css/materialize.min.css";

import Header from '../components/header';
import Footer from '../components/footer';

const Home = ({views}) => (
  <div>
    <div className="wrapper">
      <Head>
        <title>Index</title>
      </Head>
      <Header />
      <main>
        <br></br>
        <h1>Comida internacional</h1>
        <div className="slider">
          <ul>
            <li>
              <img src="car1.png" alt="Spaghetti Italiano."></img>
            </li>
            <li>
              <img src="car2.png" alt="Spaghetti Italiano."></img>
            </li>
            <li>
              <img src="car3.png" alt="Spaghetti Italiano."></img>
            </li>
            <li>
              <img src="car4.png" alt="Spaghetti Italiano."></img>
            </li>
            <li>
              <img src="car5.png" alt="Spaghetti Italiano."></img>
            </li>
          </ul>
        </div>
        <br></br>
        Luego van las imagenes de sustitucion
        <div className="wrapper2">
          <div className="img1 container">
            <img src="sus1.png" className="image"></img>
            <div className="overlay1">
              <div className="text">El mundo</div>
            </div>
          </div>
          <div className="img2 container">
            <img src="sus2.png" className="image"></img>
            <div className="overlay2">
              <div className="text">Europa</div>
            </div>
          </div>
          <div className="img3 container">
            <img src="sus3.png" className="image"></img>
            <div className="overlay3">
              <div className="text">America</div>
            </div>
          </div>
          <div className="img4 container">
            <img src="sus4.png" className="image"></img>
            <div className="overlay4">
              <div className="text">Asia</div>
            </div>
          </div>
          <div className="img5 container">
            <img src="sus5.png" className="image"></img>
            <div className="overlay5">
              <div className="text">Africa</div>
            </div>
          </div>
          <div className="img6 container">
            <img src="sus6.png" className="image"></img>
            <div className="overlay6">
              <div className="text">Oceania</div>
            </div>
          </div>
        </div>
        <br></br><br></br>
      </main>
      <Footer views = {views}/>
    </div>

    <style jsx>{`
      :global(body) {
        margin: 0;
        padding: 0;
        background: #f8f8ff;
        font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
          Helvetica, sans-serif;
      }
      a, a img {
        outline: none;
      }
      .wrapper {
        display: grid;
        grid-template-columns: auto;
        grid-template-rows: 100px auto 70px;
        grid-template-areas: 'header' 'main' 'footer';
        width: 100%;
        height: 100vh;
      }
      header{
        grid-area: header;
      }
      main{
        grid:area: main;
      }
      .wrapper2{
        display: grid;
        grid-template-areas: 'img1 img2 img3' 'img4 img5 img6';
        grid-template-columns: repeat(3, 1fr);
      }
      .wrapper2 img{
        margin: auto;
      }
      .img1{
        grid-area: img1;
      }
      .img2{
        grid-area: img2;
      }
      .img3{
        grid-area: img3;
      }
      .img4{
        grid-area: img4;
      }
      .img5{
        grid-area: img5;
      }
      .img6{
        grid-area: img6;
      }
      footer{
        grid-area: footer;
      }
      .container {
        text-align: center;
        position: relative;
        width: 100%;
        height: 100%;
        margin: auto;
      }
      
      .image {
        height: 300px;
        margin: auto;
      }
      .overlay1 {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color: #26829b;
      }
      .overlay2 {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color: #123b6f;
      }
      .overlay3 {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color: #8dd444;
      }
      .overlay4 {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color: #0069bf;
      }
      .overlay5 {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color: #00ae48;
      }
      .overlay6 {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color: #bb133e;
      }
      .container:hover .overlay1 {
        opacity: 1;
      }
      .container:hover .overlay2 {
        opacity: 1;
      }
      .container:hover .overlay3 {
        opacity: 1;
      }
      .container:hover .overlay4 {
        opacity: 1;
      }
      .container:hover .overlay5 {
        opacity: 1;
      }
      .container:hover .overlay6 {
        opacity: 1;
      }
      .text {
        color: white;
        font-size: 40px;
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        text-align: center;
      }
      .slider {
        width: 70%;
        margin: auto;
        overflow: hidden;
      }
      
      .slider ul {
        display: flex;
        padding: 0;
        width: 400%;
        
        animation: cambio 20s infinite alternate linear;
      }
      
      .slider li {
        width: 100%;
        list-style: none;
      }
      
      .slider img {
        width: 100%;
        height: 300px;
      }
      
      @keyframes cambio {
        0% {margin-left: 0;}
        20% {margin-left: 0;}
        
        25% {margin-left: -100%;}
        45% {margin-left: -100%;}
        
        50% {margin-left: -200%;}
        70% {margin-left: -200%;}
        
        75% {margin-left: -300%;}
        100% {margin-left: -300%;}
      }      
    `}</style>
  </div>
);

Home.getInitialProps = async function() {
  const data = await import('../lib/views.json');
  return data;
};

export default Home
