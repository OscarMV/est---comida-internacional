const express = require("express");
const next = require("next");
const fs = require('fs');
const path = require('path');

const PORT = process.env.PORT || 3000;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

app
  .prepare()
  .then(() => {
    const server = express();

    server.get("/", async (req, res) => {
        let file = path.join(__dirname, '../lib/views.json');
        var views = JSON.parse(fs.readFileSync(file, 'utf8'));
        views['views'] += 1;
        fs.writeFileSync(file, JSON.stringify(views))
        return handle(req, res);
    });

    server.get("*", (req, res) => {
      return handle(req, res);
    });

    server.listen(PORT, err => {
      if (err) throw err;
      console.log(`> Ready on ${PORT}`);
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });